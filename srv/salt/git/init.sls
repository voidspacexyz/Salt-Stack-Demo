include:
  - packages

junction_app:
  git.latest:
    - target : /junction/src
    - name : https://ramaseshan@github.com/ramaseshan/junction.git
    - require:
      - pkg: install_git
