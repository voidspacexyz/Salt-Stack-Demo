{% set settings = salt['pillar.get']('junction:settings') %}
{% set env = salt['pillar.get']('junction:venv') %}
{% set code = salt['pillar.get']('junction:proj') %}

include:
  - git
  - packages
 
junction_env:
  virtualenv.managed:
    - name: {{ env }}
    - system_site_packages: False
    - require:
      - git: junction_app
      - pkg: install_virtualenv

requirements:
  pip.installed:
    - requirements: {{code }}/requirements-dev.txt
    - bin_env: {{ env }}
    - require:
      - pkg: install_pip
      - virtualenv: junction_env

settings:
  file.managed:
    - name: /junction/src/settings/dev.py
    - source: /junction/src/settings/dev.py.sample
    - require:
      - virtualenv: junction_env

#migrate:
#  module.run:
#    - name: django.command
#    - settings_module: {{ settings }}
#    - command: migrate --noinput 
#    - bin_env: /junction

junction_db:
  cmd.run:
    - name: "cd /junction && source bin/activate && cd src && python manage.py migrate --noinput && python manage.py sample_data && deactivate"

dev_server:
  cmd.run:
    - name: "cd /junction && source bin/activate && cd src && python manage.py runserver 0.0.0.0:8000 >/var/log/junction.log 2>&1 & && deactivate"
