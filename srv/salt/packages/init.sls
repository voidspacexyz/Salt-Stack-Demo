install_git:
  pkg.installed:
    - name: git

install_nginx:
  pkg.installed:
    - name: nginx

install_devel:
  pkg.installed:
    - name: python-devel

install_virtualenv:
  pkg.installed:
    - name: python-virtualenv
    - require:
      - pkg: install_devel

install_pip:
  pkg.installed:
    - name: python-pip

